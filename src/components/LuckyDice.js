import { Component } from "react";
import diceImg from '../assets/images/dice.png';
import dice1 from '../assets/images/1.png';
import dice2 from '../assets/images/2.png';
import dice3 from '../assets/images/3.png';
import dice4 from '../assets/images/4.png';
import dice5 from '../assets/images/5.png';
import dice6 from '../assets/images/6.png';
import '../App.css'

class LuckyDice extends Component {
     constructor(props) {
          super(props);
          this.state = {
               imgResult: diceImg,
               value: ''
          }
     }

     onBtnNem = () => {
          let number = Math.floor(Math.random() * 6 + 1);
          let result = null;

          switch (number) {
               case 1: {
                    result = dice1;
                    break;
               }
               case 2: {
                    result = dice2;
                    break;
               }
               case 3: {
                    result = dice3;
                    break;
               }
               case 4: {
                    result = dice4;
                    break;
               }
               case 5: {
                    result = dice5;
                    break;
               }
               default: {
                    result = dice6;
               }
          }

          this.setState({
               imgResult: result,
               value: `Kết quả ném: ${number}`
          })
     }

     render() {
          return (
               <>
                    
                <div>
                     <button onClick={this.onBtnNem}>Ném</button>
                </div>
                <div>
                     <img src={this.state.imgResult} alt='dice-result' width='25%'></img>
                     <p >{this.state.value}</p>
                </div>
                    
               </>
          )
     }
}

export default LuckyDice;
